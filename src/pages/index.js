import * as React from "react";
import { useState } from "react";
import { Client } from "@heroiclabs/nakama-js";
import { Button, Typography } from "@mui/material";

import "@fontsource/roboto/300.css";
import "@fontsource/roboto/400.css";
import "@fontsource/roboto/500.css";
import "@fontsource/roboto/700.css";

const IndexPage = () => {
  const [log, setLog] = useState([]);

  // find match using matchmaker api
  const findMatch = React.useCallback(async ({ session, socket }) => {
    const username = session.username;
    const query = "*";
    const matchSize = 2;

    let _start = new Date().getTime();

    // match found handler
    socket.onmatchmakermatched = (matched) => {
      const _now = new Date().getTime();

      const _me = matched.self.presence.username;
      const _enemy = matched.users
        .filter((u) => u.presence.username !== _me)
        .map((u) => u.presence.username)[0];
      const _message =
        _me + " matched with " + _enemy + " after " + (_now - _start) + "ms";
      setLog((l) => [_message, ...l]);
    };

    // add to matchmaker pool
    await socket.addMatchmaker(query, matchSize, matchSize);
    _start = new Date().getTime();
    const message = username + " added to matchmaker pool...";
    setLog((l) => [message, ...l]);
  }, []);

  const loginUser = async (username) => {
    const client = new Client("defaultkey", "127.0.0.1", 7350, false, 20000);
    const email = username + "@email.com";
    const password = "Password1";
    const createUser = false;
    try {
      const socket = client.createSocket();

      const session = await client.authenticateEmail(
        email,
        password,
        createUser,
        username
      );
      await socket.connect(session, true);

      setLog((l) => [username + " login success", ...l]);
      return {
        session: session,
        socket: socket,
      };
    } catch (err) {
      console.error(err);
      return Promise.reject();
    }
  };

  // start batch login
  const startLogin = React.useCallback(async () => {
    const count = 2;
    const loggedUsers = [];
    let batch = [];

    for (let i = 1; i <= count; i++) {
      const pad = i.toString().padStart(5, "0");
      const username = "user_" + pad;
      batch.push(loginUser(username));
      if (batch.length > 50) {
        const _users = await Promise.all(batch);
        loggedUsers.push(..._users);
        batch = [];
      }
    }

    if (batch.length > 0) {
      const _users = await Promise.all(batch);
      loggedUsers.push(..._users);
      batch = [];
    }

    loggedUsers.forEach(findMatch);
  }, [findMatch]);

  return (
    <main>
      <Button onClick={startLogin}>Start</Button>
      {log.map((text, idx) => (
        <Typography
          key={idx}
          variant="caption"
          display="block"
          gutterBottom
          borderBottom={1}
        >
          {text}
        </Typography>
      ))}
    </main>
  );
};

export default IndexPage;
